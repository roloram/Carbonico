'''
Apr 13, 2014

Copyright (C) 2014 Rolando Ramos (roloram@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>
'''
from PyQt4 import QtGui, QtCore
import os
from carbonico.backup.backup import Backup
import Queue


class MainWidget(QtGui.QWidget):

    SET_DESTINATION_DIR = 'DestinationDirectory'
    SET_BACKUP_DIR = 'BackupDirectories'
    SET_BACKUP_DIR_PROPERTY = "Directory"
    last_backup_dir = ''
    settings = QtCore.QSettings('carbono', 'carbonico')
    backup_thread = None

    def closeEvent(self, event):
        if self.backup_thread.is_alive():
            reply = QtGui.QMessageBox.question(
                                self, 'Message', 'Are you sure to quit?',
                                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def save_backup_directories(self):
        self.settings.remove(self.SET_BACKUP_DIR)
        if self.lst_backup.count() > 0:
            self.settings.beginWriteArray(self.SET_BACKUP_DIR)
            for  i in range(self.lst_backup.count()):
                self.settings.setArrayIndex(i)
                self.settings.setValue(self.SET_BACKUP_DIR_PROPERTY,
                                       self.lst_backup.item(i).text())
            self.settings.endArray()

    def load_configuration_data(self):
        size = self.settings.beginReadArray(self.SET_BACKUP_DIR)
        for  i in range(size):
            self.settings.setArrayIndex(i)
            self.lst_backup.addItem(str(self.settings.value(
                                                self.SET_BACKUP_DIR_PROPERTY,
                                                type=str)))
        self.settings.endArray()
        self.txt_destination.setText(str(
                            self.settings.value(self.SET_DESTINATION_DIR,
                                                type=str)))

    def btn_destination_clicked(self):
        current = self.txt_destination.text()
        if current == "" or not os.path.exists(current):
            current = os.path.expanduser('~')
        selected = QtGui.QFileDialog.getExistingDirectory(self,
                                            'Destination Directory', current)
        self.txt_destination.setText(selected)
        self.settings.setValue(self.SET_DESTINATION_DIR,
                               QtCore.QVariant(selected))

    def btn_add_backup_directory_clicked(self):
        if (self.last_backup_dir == ""
        or not os.path.exists(self.last_backup_dir)):
            self.last_backup_dir = os.path.expanduser('~')
        self.last_backup_dir = QtGui.QFileDialog.getExistingDirectory(self,
                                                         'Backup Directory',
                                                         self.last_backup_dir)
        self.lst_backup.addItem(self.last_backup_dir)
        self.save_backup_directories()

    def btn_del_backup_directory_clicked(self):
        self.lst_backup.takeItem(self.lst_backup.currentRow())
        self.save_backup_directories()

    def enable_configuration_buttons(self, enable):
        self.btn_backup.setEnabled(enable)
        self.btn_add_backup_directory.setEnabled(enable)
        self.btn_del_backup_directory.setEnabled(enable)
        self.btn_destination.setEnabled(enable)

    def btn_backup_clicked(self):
        self.tmrQueue.start(1000)
        self.txtQueue.clear()
        self.enable_configuration_buttons(False)
        backup_directories = []
        for i in range(self.lst_backup.count()):
            print(self.lst_backup.item(i).text())
            backup_directories.append(str(self.lst_backup.item(i).text()))
        self.backup_thread = Backup(backup_directories,
                                    str(self.txt_destination.text()),
                                    self.queue)
        self.backup_thread.start()

    def tmrQueue_timeout(self):
        while self.queue.qsize():
            try:
                msg = self.queue.get(0)
                self.txtQueue.append(str(msg))
            except Queue.Empty:
                pass
        if not self.backup_thread.is_alive():
            self.enable_configuration_buttons(True)

    def initUI(self):
        self.queue = Queue.Queue()
        self.tmrQueue = QtCore.QTimer()
        self.tmrQueue.timeout.connect(self.tmrQueue_timeout)
        self.vbox = QtGui.QVBoxLayout(self)
        self.lbl_source = QtGui.QLabel('Backup Directories: ')

        self.vbox.addWidget(self.lbl_source)
        self.hbox_backup = QtGui.QHBoxLayout()
        self.lst_backup = QtGui.QListWidget()
        self.hbox_backup.addWidget(self.lst_backup)

        self.vbox_backup_buttons = QtGui.QVBoxLayout()
        self.btn_add_backup_directory = QtGui.QPushButton(
                                    self.style().standardIcon(
                                    QtGui.QStyle.SP_FileDialogNewFolder), '')
        self.btn_add_backup_directory.clicked.connect(
                                    self.btn_add_backup_directory_clicked)
        self.vbox_backup_buttons.addWidget(self.btn_add_backup_directory)
        self.btn_del_backup_directory = QtGui.QPushButton(
                                    self.style().standardIcon(
                                    QtGui.QStyle.SP_DialogDiscardButton), '')
        self.btn_del_backup_directory.clicked.connect(
                                    self.btn_del_backup_directory_clicked)
        self.vbox_backup_buttons.addWidget(self.btn_del_backup_directory)
        self.hbox_backup.addLayout(self.vbox_backup_buttons)
        self.vbox.addLayout(self.hbox_backup)

        self.hbox_destination = QtGui.QHBoxLayout()
        self.lbl_destination = QtGui.QLabel('Destination: ')
        self.hbox_destination.addWidget(self.lbl_destination)
        self.txt_destination = QtGui.QLineEdit()
        self.hbox_destination.addWidget(self.txt_destination)
        self.btn_destination = QtGui.QPushButton(self.style().standardIcon(
                                            QtGui.QStyle.SP_DirOpenIcon), '')
        self.btn_destination.clicked.connect(self.btn_destination_clicked)
        self.hbox_destination.addWidget(self.btn_destination)
        self.vbox.addLayout(self.hbox_destination)

        self.hbox_buttons = QtGui.QHBoxLayout()
        self.btn_backup = QtGui.QPushButton('Backup')
        self.btn_backup.clicked.connect(self.btn_backup_clicked)
        self.hbox_buttons.addWidget(self.btn_backup)
        self.btn_quit = QtGui.QPushButton('Quit')
        self.btn_quit.clicked.connect(self.close)
        self.hbox_buttons.addWidget(self.btn_quit)

        self.vbox.addLayout(self.hbox_buttons)

        self.hbox_queue = QtGui.QHBoxLayout()
        self.vbox_ref = QtGui.QVBoxLayout()
        self.vbox_ref.addWidget(QtGui.QLabel('Reference:'))
        self.vbox_ref.addWidget(QtGui.QLabel('DP=Dir Processing'))
        self.vbox_ref.addWidget(QtGui.QLabel('DC=Dir Creating'))
        self.vbox_ref.addWidget(QtGui.QLabel('DD=Dir Deleting'))
        self.vbox_ref.addWidget(QtGui.QLabel('FC=File Copying'))
        self.vbox_ref.addWidget(QtGui.QLabel('FU=File Updating'))
        self.vbox_ref.addWidget(QtGui.QLabel('FD=Fiel Deleting'))
        self.hbox_queue.addLayout(self.vbox_ref)

        self.txtQueue = QtGui.QTextEdit()
        self.hbox_queue.addWidget(self.txtQueue)
        self.vbox.addLayout(self.hbox_queue)

        self.setWindowTitle('Carbon Copy')
        self.load_configuration_data()
        self.showMaximized()

    def __init__(self):
        super(MainWidget, self).__init__()
        self.initUI()
