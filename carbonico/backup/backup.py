'''
Apr 13, 2014

Copyright (C) 2014 Rolando Ramos (roloram@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>
'''
import os
from carbonico.fs.file import File
from carbonico.fs.directory import Directory
import threading


class Backup(threading.Thread):

    def __init__(self, backup_paths, destination_path, queue):
        threading.Thread.__init__(self)
        self.backup_paths = backup_paths
        self.destination_path = destination_path
        self.queue = queue

    def run(self):
        self.queue.put('Starting Backup')
        for path in self.backup_paths:
            self.queue.put('DP: {}'.format(path))
            destination_path_aux = os.path.join(self.destination_path,
                                                os.path.split(path)[1])
            if not os.path.exists(destination_path_aux):
                os.mkdir(destination_path_aux)
            self.backup_directory(path, destination_path_aux, 1)
        self.queue.put('Backup completed.')

    def backup_directory(self, path, destination_path, indent):
        back_dirs, back_files = self.get_directory_content(path)
        dest_dirs, dest_files = self.get_directory_content(destination_path)
        indented = indent * 4 * '.'
        # Files
        for key, bfile in back_files.items():
            if key not in dest_files:
                self.queue.put('{}FC: {}'.format(indented, key))
                bfile.copy(destination_path)
            else:
                if not bfile == dest_files[key]:
                    self.queue.put('{}FU: {}'.format(indented, key))
                    bfile.copy(destination_path)
                del dest_files[key]

        for key, dfile in dest_files.items():
            self.queue.put('{}FD: {}'.format(indented, key))
            dfile.delete()

        # Directories
        ddir = None
        for key, bdir in back_dirs.items():
            if key not in dest_dirs:
                self.queue.put('{}DC: {}'.format(indented,
                                                                 key))
                ddir = Directory(bdir.name, destination_path)
                ddir.create()
            else:
                ddir = dest_dirs[key]
                del dest_dirs[key]
            self.queue.put('{}DP: {}'.format(indented, key))
            self.backup_directory(bdir.full_path(),
                                    ddir.full_path(),
                                    indent + 1)

        for key, ddir in dest_dirs.items():
            self.queue.put('{}DD: {}'.format(indented, key))
            ddir.delete()

    def get_directory_content(self, path):
        dirs = {}
        files = {}
        if os.path.exists(path):
            for f in os.listdir(path):
                if os.path.isfile(os.path.join(path, f)):
                    files[f] = File(name=f, path=path)
                else:
                    dirs[f] = Directory(name=f, path=path)
        return dirs, files
