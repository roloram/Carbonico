'''
Mar 27, 2014

Copyright (C) 2014 Rolando Ramos

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>
'''
import unittest
import os


class BackupTest(unittest.TestCase):

    def clean(self):
        if os.path.isdir(self.path):
            for root, dirs, files in os.walk(self.path, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
                for name in dirs:
                    os.rmdir(os.path.join(root, name))
            os.rmdir(self.path)

    def setUp(self):
        self.dir_test = 'dir_test'
        self.path = os.path.join(os.path.expanduser('~'), 'test')
        self.full_path = os.path.join(self.path, self.dir_test)
        self.clean()
        os.mkdir(self.path)

    def tearDown(self):
        self.clean()

    def test_backup(self):
        pass
        #d = Directory(self.dir_test, self.path)
        #self.assertEqual(d.full_path(), self.full_path)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
