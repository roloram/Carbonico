'''
Jan 27, 2014

Copyright (C) 2014 rolando

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>
'''
import os


class Directory(object):

    def __init__(self, name="", path=""):
        self.name = name
        self.path = path

    def full_path(self):
        return os.path.join(self.path, self.name)

    def __repr__(self):
        return "Directory: {}".format(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def copy(self, backup_directory):
        os.mkdir(os.path.join(self.path, backup_directory))

    def create(self):
        os.mkdir(os.path.join(self.path, self.name))

    def delete(self):
        for root, dirs, files in os.walk(self.full_path(), topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        os.rmdir(self.full_path())
