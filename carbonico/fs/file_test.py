'''
Jan 30, 2014

Copyright (C) 2014 rolando

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>
'''
import unittest
import os
from carbonico.fs.file import File


class Test(unittest.TestCase):

    def clean(self):
        if os.path.isdir(self.path):
            for root, dirs, files in os.walk(self.path, topdown=False):
                for name in files:
                    os.remove(os.path.join(root, name))
                for name in dirs:
                    os.rmdir(os.path.join(root, name))
            os.rmdir(self.path)

    def setUp(self):
        self.file_test = 'file_test.txt'
        self.path = os.path.join(os.path.expanduser('~'), 'test')
        self.backup_path = os.path.join(self.path, 'backup')
        self.full_path = os.path.join(self.path, self.file_test)
        self.backup_full_path = os.path.join(self.backup_path, self.file_test)
        self.clean()
        os.mkdir(self.path)
        os.mkdir(self.backup_path)

    def tearDown(self):
        self.clean()

    def create_file(self):
        f = open(self.full_path, 'w')
        f.write('test')
        f.close()

    def test_full_path(self):
        f = File(self.file_test, self.path)
        self.assertEqual(f.full_path(), self.full_path)

    def test_copy(self):
        self.create_file()
        f = File(self.file_test, self.path)
        f.copy(self.backup_path)
        self.assertTrue(os.path.isfile(self.backup_full_path))

    def test_delete(self):
        self.create_file()
        f = File(self.file_test, self.path)
        f.delete()
        self.assertFalse(os.path.isdir(self.full_path))

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
