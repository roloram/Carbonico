'''
Nov 20, 2013

Copyright (C) 2013 rolando

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>
'''
import os
import shutil
import datetime


class File(object):

    def __init__(self, name="", path=""):
        self.name = name
        self.path = path

    def full_path(self):
        return os.path.join(self.path, self.name)

    def last_modification_date(self):
        return datetime.datetime.fromtimestamp(
                                        os.path.getmtime(self.full_path()))

    def size(self):
        return os.path.getsize(self.full_path())

    def copy(self, backup_directory):
        shutil.copy2(self.full_path(), backup_directory)

    def delete(self):
        os.remove(self.full_path())

    def __repr__(self):
        return "File:  {}".format(self.name)

    def __eq__(self, other):
        return (self.name == other.name
                and (self.last_modification_date() -
                     other.last_modification_date()) <
                     datetime.timedelta(seconds=10)
                and (other.last_modification_date() -
                     self.last_modification_date()) <
                     datetime.timedelta(seconds=10)
                and self.size() == other.size())
